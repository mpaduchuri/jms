package test.com.jms.listener;

import static org.junit.Assert.*;

import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jms.listener.MessageListener;

public class MessageListenerTest {
	
	

	private TextMessage message;
	
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testOnMessage() {
		 MessageListener listener = new MessageListener();
		 listener.onMessage(message);
		 assertNull(message);
		 
	}

}
