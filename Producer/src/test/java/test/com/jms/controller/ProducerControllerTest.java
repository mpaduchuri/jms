package test.com.jms.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.jms.producer.controller.ProducerController;
import com.jms.producer.model.Vendor;

public class ProducerControllerTest {
	private Vendor vendor;
	private Model model;
	private ProducerController producerController;
	private ApplicationContext context;
	
	@Before
	public void setUp() throws Exception {
		context	= new ClassPathXmlApplicationContext("spring/application-config.xml");
		producerController = (ProducerController) context.getBean("producerController");
		vendor = new Vendor();
		vendor.setVendorName("DHL");
		vendor.setFirstName("John");
		vendor.setLastName("Deer");
		vendor.setAddress("123 Str.");
		vendor.setCity("Hof");
		vendor.setState("bavaria");
		vendor.setZipCode("12345");
		vendor.setEmail("John@DHL.COM");
		vendor.setPhoneNumber("123-111-1234");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRenderVendorPage() {
		assertEquals("index", producerController.renderVendorPage(vendor, model));
	}

	@Test
	public void testProcessRequest() {
		ModelAndView mv = producerController.processRequest(vendor, model);
		assertEquals("index", mv.getViewName());
	}

}
