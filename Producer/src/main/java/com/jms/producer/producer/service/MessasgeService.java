package com.jms.producer.producer.service;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.jms.producer.model.Vendor;
import com.jms.producer.sender.MessageSender;

@Component
public class MessasgeService {
	private static Logger logger = LogManager.getLogger(MessasgeService.class.getName());
	@Autowired
	MessageSender messageSender;
	
	public void process(Vendor vendor) {
		 Gson gson = new Gson();
		 String json = gson.toJson(vendor);
		 messageSender.send(json);
		
	}

}
