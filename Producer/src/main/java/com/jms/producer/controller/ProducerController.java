package com.jms.producer.controller;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jms.producer.model.Vendor;
import com.jms.producer.producer.service.MessasgeService;

@Controller
public class ProducerController {
	private static Logger logger = LogManager.getLogger(ProducerController.class.getName());
	
	@Autowired
	private MessasgeService messageService;
	@RequestMapping("/")
	public String renderVendorPage(Vendor vendor, Model model){
		logger.info("Rendering index jsp");
		return "index";
	}
	
	@RequestMapping(value="/vendor",method=RequestMethod.POST)
	public ModelAndView processRequest(@ModelAttribute("vendor") Vendor vendor, Model model)
	{
		logger.info("Processing Vendor Object");
		messageService.process(vendor);
		ModelAndView mv = new ModelAndView();
		mv.setViewName("index");
		mv.addObject("message","Vendor added successfully");
		vendor = new Vendor();
		mv.addObject("vendor",vendor);
		return mv;
	}
}
